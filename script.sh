#!/bin/sh
# resner's shellscript template. licensed under 0BSD.

program_name="program_name"
program_version="0"
program_description="Shellscript template."
program_help="$program_name v$program_version - $program_description
$0 [flags] ...
	-h		show this.
	-v		show version.
	-q		enable quiet mode."

quiet=1

print(){
# message
	 printf "%s\n" "$1"
}
print_normal(){
# message
	 [ $quiet -eq 0 ] || print "$1"
}
print_important(){
# message
	print "IMPORTANT: $1"
}
print_error(){
# message
	print "$1" 1>&2
}
fatal(){
# message, error code
	print_error "FATAL: $1"
	exit $2
}

while getopts "hvq" flag; do
	case $flag in
		h) print_normal "$program_help" ;;
		v) print_normal "$program_version" ;;
		q) quiet=0 ;; 
		\?) print_error "Unknown flag: $flag." ;;
	esac
done
